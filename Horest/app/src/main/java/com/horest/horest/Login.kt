package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_ayuda.*
import kotlinx.android.synthetic.main.activity_ayuda.view.*
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {
    var n = 100
    var i = 0
    var datos2 = arrayOfNulls<String>(n)
    var datos3 = arrayOfNulls<String>(n)
    var datos4 = arrayOfNulls<String>(n)
    var datos5 = arrayOfNulls<String>(n)
    var datos6 = arrayOfNulls<String>(n)
    var datos7 = arrayOfNulls<String>(n)
    var datos8 = arrayOfNulls<String>(n)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombres", "")
            val apellidos = bundleLibriDeNull.getString("key_apellido", "")
            val genero = bundleLibriDeNull.getString("key_genero", "")
            val edad = bundleLibriDeNull.getString("key_ed", "")
            val us = bundleLibriDeNull.getString("key_us", "")
            val contrasena1 = bundleLibriDeNull.getString("key_contra1", "")
            val contrasena2 = bundleLibriDeNull.getString("key_contra2", "")
            if (i < n) {
                datos2[i] = nombres.toString()
                datos3[i] = apellidos.toString()
                datos4[i] = genero.toString()
                datos5[i] = edad.toString()
                datos6[i] = us.toString()
                datos7[i] = contrasena1.toString()
                datos8[i] = contrasena2.toString()
            }
            i++
        }
        btnIngresar.setOnClickListener {
            val i = 0
            if (edtUser.text.toString().isEmpty()) {
                Toast.makeText(this, "Es necesario ingresar el usuario", Toast.LENGTH_SHORT).show()
            }
            if (edtContra.text.toString().isEmpty()) {
                Toast.makeText(this, "Es necesario ingresar la contraseña", Toast.LENGTH_SHORT)
                    .show()
            }
            if (i < n) {
                if (datos6[i].equals(edtUser.text.toString()) && datos7[i].equals(edtContra.text.toString())) {
                    val i = Intent(Intent(this, ViewInstructions::class.java))
                    i.putExtra("USUARIO", edtUser.text.toString())
                    startActivity(i)
                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
                } else if (i == n) {
                    i + 1
                } else if (edtUser.text.toString() == "Admin" && edtContra.text.toString() == "Admin") {
                    val i = Intent(Intent(this, ViewInstructions::class.java))
                    i.putExtra("USUARIO", edtUser.text.toString())
                    startActivity(i)
                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Usuario no encontrado", Toast.LENGTH_SHORT).show()
                }
            }
        }
        btnRegistro.setOnClickListener {
            startActivity(Intent(this, Registro::class.java))
        }
        btnAyuda.setOnClickListener {
            val bottomSheet = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
            val view = LayoutInflater.from(this).inflate(R.layout.activity_ayuda, contenedor)
            view.dato.text = "Recuerda primero Registrar su Usuario y Contraseña o Ingresa con ' Admin ' & ' Admin '"
            bottomSheet.setContentView(view)
            bottomSheet.show()
        }
    }
}