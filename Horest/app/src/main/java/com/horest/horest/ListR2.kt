package com.horest.horest

class ListR2(nombre: String, direccion: String, rating: Float, foto: Int) {

    var nombre = ""
    var direccion = ""
    var rating = 0.0F
    var foto = 0

    init {
        this.nombre = nombre
        this.direccion = direccion
        this.rating = rating
        this.foto = foto
    }
}
